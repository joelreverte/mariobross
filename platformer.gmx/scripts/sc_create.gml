grav = 0.2;
hsp = 0;
vsp = 0;

jumpspeed = 8;
movespeed = 4;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;
  
//Init variables
key_left  = 0;
key_right = 0;
key_jump = false;
left_release = false;
right_release = false;
